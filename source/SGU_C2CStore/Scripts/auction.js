﻿$(function () {
    var auctionHub = $.connection.auctionHub;

    $.connection.hub.start().done(function () {
        $('.btnclass').click(function () {
            var btnBid = document.querySelector("#btnBid").value;
            auctionHub.server.bid(this.id,btnBid);
            alert(this.id);
        });
    });

    auctionHub.client.updateBidResults = function (id, auction) {
        var Price = $("span[data-itemid='" + id + "']");
        Price[0].textContent = auction;
    }

    auctionHub.client.joinBid = function (auctions) {
        joinBid(auctions);
    }

    function joinBid(auctions) {
        for (i = 0; i <= auctions.length - 1; i++) {
            var BidFor = $("span[data-itemid='" + auctions[i].Id + "']");
            BidFor[0].textContent = auctions[i].CurrentPrice;
        }
    }
});

