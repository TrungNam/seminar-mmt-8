﻿using Microsoft.AspNet.SignalR;
using SGU_C2CStore.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using Microsoft.AspNet.Identity;

namespace SGU_C2CStore.Hubs
{
    [Authorize]
    public class AuctionHub : Hub
    {
        
        private static List<AuctionProduct> auctions = new List<AuctionProduct>();
        private static ApplicationDbContext db = new ApplicationDbContext();
       
        public void Bid(int id, int price)
        {
            // AddVote tabulates the vote         
            var auc = AddBid(id,price);

            // Clients.All.updateVoteResults notifies all clients that someone has voted and the page updates itself to relect that 
            Clients.All.updateBidResults(id,auc);
        }
        private static int AddBid(int id,int price)
        {       
            var item = db.AuctionProducts.Find(id);
            var result = item.CurrentPrice;
            if (price > item.CurrentPrice)
            {
                // If the item is in VoteItems, we're tracking it, so just increment and return
                var auction = auctions.Find(v => v.Id == id);
                if (auction != null)
                {
                    auction.CurrentPrice = price;
                    item.CurrentPrice = price;
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                    result = item.CurrentPrice;
                }
                // If the item wasn't in VoteItems, it's the first time someone voted for it. 
                // Add it to VoteItems and increment from zero
                else
                {
                    item.CurrentPrice = price;
                    auctions.Add(item);
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                    result = item.CurrentPrice;
                }
            }
            return result;

        }
        public override Task OnConnected()
        {
         
            // Send voting history to caller only so they can see an updated view of the votes   
            Clients.Caller.joinBid(auctions.ToList());
            return base.OnConnected();
        }
    }
}
    