﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SGU_C2CStore.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("Connection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // IMPORTANT
            modelBuilder.Entity<ApplicationUser>().ToTable("AspNetUsers");
            modelBuilder.Entity<ApplicationRole>().ToTable("AspNetRoles");
            modelBuilder.Entity<ApplicationUserLogin>().ToTable("AspNetUserLogins");
            modelBuilder.Entity<ApplicationUserRole>().ToTable("AspNetUserRoles");
            modelBuilder.Entity<ApplicationUserClaim>().ToTable("AspNetUserClaims");
            modelBuilder.Entity<Product>().HasKey(e => e.Id);
            modelBuilder.Entity<Category>().HasKey(e => e.Id);
            modelBuilder.Entity<Comment>().HasKey(e => e.Id);
            modelBuilder.Entity<Order>().HasKey(e => e.Id);
            modelBuilder.Entity<OrderDetail>().HasKey(e => e.Id);
            modelBuilder.Entity<Photo>().HasKey(e => e.Id);
            modelBuilder.Entity<Product>().HasMany(e => e.Photos).WithRequired().HasForeignKey(e => e.ProductId);
            modelBuilder.Entity<Product>().HasMany(e => e.Comments).WithRequired().HasForeignKey(e => e.ProductId);
            modelBuilder.Entity<Category>().HasMany(e => e.Products).WithRequired().HasForeignKey(e => e.CategoryId);
            modelBuilder.Entity<Order>().HasMany(e => e.OrderDetails).WithRequired().HasForeignKey(e => e.OrderId);
            modelBuilder.Entity<ApplicationUser>().HasMany(e => e.Orders).WithRequired().HasForeignKey(e => e.UserId);
            modelBuilder.Entity<AuctionProduct>().HasMany(e => e.Bids).WithRequired().HasForeignKey(e => e.AuctionId);


            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<AuctionProduct> AuctionProducts { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Bid> Bids { get; set; }
        public IEnumerable<ApplicationUser> AspNetUsers { get; internal set; }
        public IEnumerable<ApplicationRole> AspNetRoles { get; internal set; }
        public IEnumerable<ApplicationUserLogin> AspNetUserLogins { get; internal set; }
        public IEnumerable<ApplicationUserRole> AspNetUserRoles { get; internal set; }
        public IEnumerable<ApplicationUserClaim> AspNetUserClaims { get; internal set; }

    }
}