﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGU_C2CStore.Models
{
    public class Bid
    {
        public virtual int Id { get; set; }
        public virtual ApplicationUser UserBid { get; set; }

        public virtual AuctionProduct Auction { get; set; }
        public virtual int AuctionId { get; set; }
        public virtual int Price { set; get; }
        public virtual DateTime TimeBid { set; get; }
    }
}