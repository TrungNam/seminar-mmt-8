﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;

namespace SGU_C2CStore.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public virtual string FullName { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual DateTime Birthday { get; set; }

        public virtual DateTime DateRegister { get; set; }
        public virtual string Address { get; set; }
        public virtual decimal Score { get; set; }

        public virtual decimal Money { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<Bid> Bids { get; set; }
        public virtual ICollection<AuctionProduct> AuctionProducts { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
    public class ApplicationRole : IdentityRole
    {

    }
    public class ApplicationUserRole : IdentityUserRole
    {

    }
    public class ApplicationUserLogin : IdentityUserLogin
    {

    }
    public class ApplicationUserClaim : IdentityUserClaim
    {

    }
  

}