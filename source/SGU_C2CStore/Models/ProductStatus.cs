﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SGU_C2CStore.Models
{
    public enum ProductStatus
    {
        [Display(Name = "Đã bị từ chối")]
        Denied,
        [Display(Name = "Đang chờ xét duyệt")]
        Checking,
        [Display(Name = "Đang bán")]
        Selling,
        [Display(Name = "Đã bán")]
        Sold 
    }
}