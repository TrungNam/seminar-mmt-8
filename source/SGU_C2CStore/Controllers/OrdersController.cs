﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SGU_C2CStore.Models;
using Microsoft.AspNet.Identity;
using Rotativa;

namespace SGU_C2CStore.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Orders
        public ActionResult Index()
        {
            var orders = db.Orders.Include(o => o.User);
            return View(orders.ToList());
        }

        // GET: Orders/Details/5
        public ActionResult Details(string userId,int orderId)
        {
            ApplicationUser user = db.Users.FirstOrDefault(u => u.Id == userId);
            ViewBag.User = user;
            Order order = db.Orders.Find(orderId);
            List<OrderDetail> details = db.OrderDetails.Where(p => p.OrderId == orderId).ToList();
            ViewBag.Details = details;
            ViewBag.Order = order;
            return View();
        }
        public ActionResult OrderRating()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            string userId = User.Identity.GetUserId();
            ApplicationUser user = db.Users.FirstOrDefault(u => u.Id == userId);
            List<Order> order = db.Orders.Where(i => i.UserId == userId).ToList();
            List<OrderDetail> details = db.OrderDetails.Where(i => i.Order.UserId == userId).ToList();
            ViewBag.ListOrderDetails = details;
            ViewBag.ListOrders = order;
            ViewBag.Account = user;
            return View();
        }
        public ActionResult Rating(string Id, int IdOrder)
        {
            Decimal count = 0;
            int i = 0;
            ApplicationDbContext db = new ApplicationDbContext();
           
            OrderDetail od = db.OrderDetails.FirstOrDefault(o => o.Id == IdOrder);
            switch (Id)
            {
                case "1":
                    od.Rating = 1;
                    db.Entry(od).State = EntityState.Modified;
                    db.SaveChanges();
                    ViewBag.TB = 1;
                    break;
                case "2":
                    od.Rating = 2;
                    db.Entry(od).State = EntityState.Modified;
                    db.SaveChanges();
                    ViewBag.TB = 2;
                    break;
                case "3":
                    od.Rating = 3;
                    ViewBag.TB = 3;
                    db.Entry(od).State = EntityState.Modified;
                    db.SaveChanges();
                    break;
                case "4":
                    od.Rating = 4;
                    ViewBag.TB = 4;
                    db.Entry(od).State = EntityState.Modified;
                    db.SaveChanges();
                    break;
                case "5":
                    od.Rating = 5;
                    ViewBag.TB = 5;
                    db.Entry(od).State = EntityState.Modified;
                    db.SaveChanges();
                    break;
                case "6":
                    od.Rating = 6;
                    ViewBag.TB = 6;
                    db.Entry(od).State = EntityState.Modified;
                    db.SaveChanges();
                    break;
                case "7":
                    od.Rating = 7;
                    ViewBag.TB = 7;
                    db.Entry(od).State = EntityState.Modified;
                    db.SaveChanges();
                    break;
                case "8":
                    od.Rating = 8;
                    ViewBag.TB = 8;
                    db.Entry(od).State = EntityState.Modified;
                    db.SaveChanges();
                    break;
                case "9":
                    od.Rating = 9;
                    ViewBag.TB = 9;
                    db.Entry(od).State = EntityState.Modified;
                    db.SaveChanges();
                    break;
                case "10":
                    od.Rating = 10;
                    ViewBag.TB = 10;
                    db.Entry(od).State = EntityState.Modified;
                    db.SaveChanges();
                    break;
                
            }
                ApplicationUser user = db.Users.FirstOrDefault(p => p.Id == od.Product.User.Id);
                List<OrderDetail> detail = db.OrderDetails.Where(p => p.Product.User.Id == od.Product.User.Id).ToList();
                foreach (OrderDetail item in detail)
                {
                    if (item.Rating != 0)
                    {
                        i++;
                        count = count + item.Rating;
                    }
                }
                if (i != 0)
                {
                    user.Score = count / i;
                    db.SaveChanges();
                }
            return RedirectToAction("OrderRating");
        }
        // GET: Orders/Create

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.


        
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            // Fetch the userprofile
            ApplicationUser user = db.Users.FirstOrDefault(u => u.Id == userId);
            Order order = new Order();
            List<Product> cart = Session["Cart"] as List<Product>;
            int Total = 0;
            order.ShipAddress = user.Address;
            order.OrderTime = DateTime.Now;
            order.UserId = userId;          
            foreach(Product product in cart)
            {
                Product productdb = db.Products.Find(product.Id);
                Total = Total + product.Price;
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.Product = db.Products.FirstOrDefault(p => p.Id == product.Id);
                orderDetail.ProductPrice = product.Price;
                orderDetail.OrderId = order.Id;
                productdb.ProductStatus = ProductStatus.Sold;           
                db.OrderDetails.Add(orderDetail);
            }
            order.ShipTime= DateTime.Now;
            order.Total = Total;
            order.User = user;
            Session["Cart"] = null;
            db.Orders.Add(order);
            db.SaveChanges();
            return RedirectToAction("Details",new { orderId= order.Id , userId =user.Id});                
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "FullName", order.UserId);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OrderTime,ShipTime,ShipAddress,UserId,Total")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "FullName", order.UserId);
            return View(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult ExportPDF(string userId,int orderId)
        {
            return new ActionAsPdf("Details", new { orderId = orderId, userId = userId })
            {
                FileName = Server.MapPath("~/Assets/user/PDF/Invoice.pdf")
            };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
