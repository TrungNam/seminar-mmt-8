﻿using SGU_C2CStore.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Threading.Tasks;

namespace SGU_C2CStore.Controllers
{
    public class ShoppingCartController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        
        //
        // GET: /ShoppingCart/
        List<Product> cart;
        Product product = new Product();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddToCart(int id, string returnUrl)
        {
            if (Session["Cart"] == null)
            {
                cart = new List<Product>();
                product = db.Products.Where(i => i.Id == id).Single();
                cart.Add(product);
                Session["Cart"] = cart;
            }
            else
            {
                cart = Session["Cart"] as List<Product>;
                if (!isExist(id))
                {
                    product = db.Products.Where(i => i.Id == id).Single();
                    cart.Add(product);
                }
                Session["Cart"] = cart;
            }
            return RedirectToLocal(returnUrl);
        }
        public bool isExist(int id)
        {
            cart = Session["Cart"] as List<Product>;
            Product isExist = cart.Find(i => i.Id == id);
            if (isExist == null)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        public ActionResult RemoveFormCart(int id, string returnUrl)
        {
            cart = Session["Cart"] as List<Product>;
            Product product = cart.Find(i => i.Id == id);
            cart.Remove(product);
            Session["Cart"] = cart;
            return RedirectToLocal(returnUrl);
        }
       
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (returnUrl != null)
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Products");
        }
        [Authorize]
        public ActionResult Checkout()
        {
         
            return View();
        }


    }
}