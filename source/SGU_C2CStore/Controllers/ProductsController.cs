﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SGU_C2CStore.Models;
using System.Web;
using System.IO;
using Microsoft.AspNet.Identity;
using System;
using PagedList.Mvc;
using PagedList;

namespace SGU_C2CStore.Controllers
{
    public class ProductsController : Controller
    {
        public string currentURL;
        private ApplicationDbContext db = new ApplicationDbContext();
        
        // GET: Products
        public ActionResult Index(string sortOrder, int? page,string search,string currentSearch, int? catId, int? currentcateId)
        {
            ViewBag.Forgot = new ForgotPasswordViewModel();
            ViewBag.Login = new LoginViewModel();
            ViewBag.Register = new RegisterViewModel();
            List<Category> category = db.Categories.ToList();
            ViewBag.Category = category;
            ViewBag.Avatar = db.Photos.ToList();
            var auctions = db.AuctionProducts.Where(p => p.ProductStatus == ProductStatus.Selling).Include(p => p.User);
            var product = db.Products.Where(p => p.ProductStatus == ProductStatus.Selling).Include(p => p.User);
            var products = product.Except(auctions);
            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentSearch;
            }

            ViewBag.CurrentSort = sortOrder;
            if (!string.IsNullOrEmpty(search))
            {
                products = products.Where(p => p.Name.Contains(search) || p.Name.StartsWith(search));
            }
            ViewBag.CurrentSearch = search;

            if (catId == 0 || catId == null)
            {
                products = products.Where(p => p.CategoryId != catId); ;
            }
            else
            {
                products = products.Where(p => p.CategoryId == catId);
            }
            ViewBag.CurrentFilter = catId;
            ViewBag.NameSort = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSort = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.PriceSort = sortOrder == "Price" ? "Price_desc" : "Price";
            switch (sortOrder)
            {
                case "date_desc":
                    products = products.OrderByDescending(p => p.DateCreate);
                    break;
                case "name_desc":
                    products = products.OrderByDescending(p => p.Name);
                    break;
               case "Name":
                    products = products.OrderBy(p => p.Name);
                    break;
                case "Price":
                    products = products.OrderBy(p => p.Price);
                    break;
                case "Price_desc":
                    products = products.OrderByDescending(p => p.Price);
                    break;
                default:
                    products = products.OrderBy(p => p.DateCreate);
                    break;
            }

            int pageSize = 9;
            int pageNumber = (page ?? 1);
            return View(products.ToPagedList(pageNumber,pageSize));
        }

        public ActionResult Manage()
        {
            var userId = User.Identity.GetUserId();
            ViewBag.Forgot = new ForgotPasswordViewModel();
            ViewBag.Login = new LoginViewModel();
            ViewBag.Register = new RegisterViewModel();
            List<Category> category = db.Categories.ToList();
            ViewBag.Category = category;
            ViewBag.Avatar = db.Photos.ToList();
            var products = db.Products.Where(p => p.UserId== userId);
            return View(products.ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.Images = db.Photos.Where(i => i.ProductId == id).ToList();
            ViewBag.Login = new LoginViewModel();
            ViewBag.Register = new RegisterViewModel();
            ViewBag.Forgot = new ForgotPasswordViewModel();
            currentURL = Request.Url.AbsoluteUri;
            List<Category> category = db.Categories.ToList();
            ViewBag.Category = category;
            return View(product);
        }
        [Authorize]
        // GET: Products/Create
        public ActionResult Create()
        {

            IEnumerable<SelectListItem> items = db.Categories.Select(
                c => new SelectListItem
                {
                    Value = c.Id.ToString(),
                    Text= c.Name
                });

            ViewBag.CategoryId = items;
            Product product = new Product();
            
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,CategoryId,Price,Description,UserId")] Product product,IEnumerable<HttpPostedFileBase> upload)
        {            
            var userId = User.Identity.GetUserId();
            product.UserId = userId;
            if (ModelState.IsValid)
            {
                foreach (var item in upload)
                {
                    Photo photo = new Photo();
                    if (item.ContentLength > 0)
                    {
                        
                        string extension = Path.GetExtension(item.FileName);
                        var path = Path.Combine(Server.MapPath("~/Images"),product.Id+ extension);
                        item.SaveAs(path);
                        photo.URL = product.Id + extension;
                        photo.ProductId = product.Id;

                        db.Photos.Add(photo);
                    }
                }
                product.DateCreate = DateTime.Now;
                product.ProductStatus = ProductStatus.Checking;
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            IEnumerable<SelectListItem> items = db.Categories.Select(
                c => new SelectListItem
                {
                    Value = c.Id.ToString(),
                    Text = c.Name
                  
                });
            ViewBag.Login = new LoginViewModel();
            ViewBag.Register = new RegisterViewModel();
            ViewBag.Forgot = new ForgotPasswordViewModel();
            ViewBag.CategoryId = items;
            return View(product);
        }
        [Authorize]
        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", product.CategoryId);
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", product.UserId);
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,CategoryId,Price,Description,UserId")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", product.CategoryId);
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", product.UserId);
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    
    }
}
