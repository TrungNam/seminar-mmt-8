﻿
using SGU_C2CStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGU_C2CStore.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            var auctions = db.AuctionProducts;     
            var products = db.Products;

            var pro = products.Except(auctions).Where(e => e.ProductStatus == ProductStatus.Selling).ToList();
            var auc = auctions.Where(e => e.ProductStatus == ProductStatus.Selling).ToList();
            ViewBag.products = pro;
            ViewBag.auctions = auc;
            ViewBag.Login = new LoginViewModel();
            ViewBag.Register = new RegisterViewModel();
            ViewBag.Forgot = new ForgotPasswordViewModel();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.Login = new LoginViewModel();
            ViewBag.Register = new RegisterViewModel();
            ViewBag.Forgot = new ForgotPasswordViewModel();
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            ViewBag.Login = new LoginViewModel();
            ViewBag.Register = new RegisterViewModel();
            ViewBag.Forgot = new ForgotPasswordViewModel();
            return View();
        }
    }
}