﻿using Microsoft.AspNet.Identity;
using PagedList;
using SGU_C2CStore.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SGU_C2CStore.Controllers
{
    public class AuctionController : Controller
    {
        public string currentURL;
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Auction
        public ActionResult Index(string sortOrder, int? page, string search, string currentSearch, int? catId, int? currentcateId)
        {
            ViewBag.Forgot = new ForgotPasswordViewModel();
            ViewBag.Login = new LoginViewModel();
            ViewBag.Register = new RegisterViewModel();
            List<Category> category = db.Categories.ToList();
            ViewBag.Category = category;
            ViewBag.Avatar = db.Photos.ToList();

            //var all = db.Products.Where(p => p.ProductStatus == ProductStatus.Selling);
            var auctions = db.AuctionProducts.Where(p => p.ProductStatus == ProductStatus.Selling);
            //var auc = all.Except(auction);
            //var auctions = all.Except(auc);
           
            if (search != null)
            {
                page = 1;
            }
            else
            {
                search = currentSearch;
            }

            ViewBag.CurrentSort = sortOrder;
            if (!string.IsNullOrEmpty(search))
            {
                auctions = auctions.Where(p => p.Name.Contains(search) || p.Name.StartsWith(search));
            }
            ViewBag.CurrentSearch = search;

            if (catId == 0 || catId == null)
            {
                auctions = auctions.Where(p => p.CategoryId != catId); ;
            }
            else
            {
                auctions = auctions.Where(p => p.CategoryId == catId);
            }
            ViewBag.CurrentFilter = catId;
            ViewBag.NameSort = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSort = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.PriceSort = sortOrder == "Price" ? "Price_desc" : "Price";
            switch (sortOrder)
            {
                case "date_desc":
                    auctions = auctions.OrderByDescending(p => p.DateCreate);
                    break;
                case "name_desc":
                    auctions = auctions.OrderByDescending(p => p.Name);
                    break;
                case "Name":
                    auctions = auctions.OrderBy(p => p.Name);
                    break;
                case "Price":
                    auctions = auctions.OrderBy(p => p.Price);
                    break;
                case "Price_desc":
                    auctions = auctions.OrderByDescending(p => p.Price);
                    break;
                default:
                    auctions = auctions.OrderBy(p => p.DateCreate);
                    break;
            }

            int pageSize = 9;
            int pageNumber = (page ?? 1);
            return View(auctions.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult Create()
        {

            IEnumerable<SelectListItem> items = db.Categories.Select(
                c => new SelectListItem
                {
                    Value = c.Id.ToString(),
                    Text = c.Name
                });

            ViewBag.CategoryId = items;
            AuctionProduct auctions = new AuctionProduct();

            return View();
        }
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,CategoryId,Price,Description,UserId,CurrentPrice,")] AuctionProduct auctions, IEnumerable<HttpPostedFileBase> upload)
        {
            
            var userId = User.Identity.GetUserId();
            ApplicationUser user = db.Users.SingleOrDefault(e => e.Id == userId);
            if (ModelState.IsValid)
            {
                foreach (var item in upload)
                {
                    Photo photo = new Photo();
                    if (item.ContentLength > 0)
                    {

                        string extension = Path.GetExtension(item.FileName);
                        var path = Path.Combine(Server.MapPath("~/Images"), auctions.Id + extension);
                        item.SaveAs(path);
                        photo.URL = auctions.Id + extension;
                        photo.Product = db.AuctionProducts.SingleOrDefault(e => e.Id == auctions.Id);

                        db.Photos.Add(photo);
                    }
                }
                auctions.User = user;
                auctions.UserId = userId;
                auctions.DateCreate = DateTime.Now;
                auctions.ProductStatus = ProductStatus.Selling;             
                auctions.CurrentPrice = auctions.Price;
                auctions.EndTime = DateTime.Now;
                auctions.StartTime = DateTime.Now;
                db.AuctionProducts.Add(auctions);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            IEnumerable<SelectListItem> items = db.Categories.Select(
                c => new SelectListItem
                {
                    Value = c.Id.ToString(),
                    Text = c.Name

                });
            ViewBag.Login = new LoginViewModel();
            ViewBag.Register = new RegisterViewModel();
            ViewBag.Forgot = new ForgotPasswordViewModel();
            ViewBag.CategoryId = items;
            return View(auctions);
        }
        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            AuctionProduct auction = db.AuctionProducts.Find(id);
            if (id == null )
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }          
            if (auction == null)
            {
                return HttpNotFound();
            }
            ViewBag.Images = db.Photos.Where(i => i.ProductId == id).ToList();
            ViewBag.Login = new LoginViewModel();
            ViewBag.Register = new RegisterViewModel();
            ViewBag.Forgot = new ForgotPasswordViewModel();
            ViewBag.AuctionId = id;
            currentURL = Request.Url.AbsoluteUri;
            List<Category> category = db.Categories.ToList();
            ViewBag.Category = category;
            return View(auction);
        }
        public void Stop(int id)
        {
            AuctionProduct auction = db.AuctionProducts.Find(id);
            auction.ProductStatus = ProductStatus.Sold;
            db.Entry(auction).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}