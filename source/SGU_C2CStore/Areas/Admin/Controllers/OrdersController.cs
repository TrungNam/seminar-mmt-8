﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGU_C2CStore.Models;

namespace SGU_C2CStore.Areas.Admin.Controllers
{
    public class OrdersController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Admin/Orders
        public ActionResult Index()
        {
            List<OrderDetail> detail = db.OrderDetails.ToList();
            ViewBag.Order = detail;
            return View();
        }
    }
}