﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SGU_C2CStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGU_C2CStore.Areas.Admin.Controllers
{
    public class ProfileController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Admin/ProfileManager
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Manage()
        {
            ViewBag.Account = db.Users.ToList();
            return View();
        }
        public ActionResult BlockUser(string userID)
        {

            ApplicationDbContext db = new ApplicationDbContext();
            ApplicationUser user = db.Users.FirstOrDefault(u => u.Id == userID);
            if (user.LockoutEnabled == false)
            {
                user.LockoutEnabled = true;

                user.LockoutEndDateUtc = DateTime.Now;

                db.SaveChanges();
            }
            return RedirectToAction("Manage");

        }
        public ActionResult UnBlockUser(string userID)
        {
            
            ApplicationDbContext db = new ApplicationDbContext();
            ApplicationUser user = db.Users.FirstOrDefault(u => u.Id == userID);
            if (user.LockoutEnabled == true)
            {
                user.LockoutEnabled = false;

                user.LockoutEndDateUtc = DateTime.Now;

                db.SaveChanges();
            }
            return RedirectToAction("Manage");
        }
    }
}