﻿namespace SGU_C2CStore.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            var store = new UserStore<ApplicationUser>(context);
            var manager = new UserManager<ApplicationUser>(store);
            var rolestore = new RoleStore<ApplicationRole>(context);
            var rolemanager = new RoleManager<ApplicationRole>(rolestore);
            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                var role = new ApplicationRole { Name = "Admin" };
                rolemanager.Create(role);
            }


            if (!context.Roles.Any(r => r.Name == "SuperAdmin"))
            {
                var role = new ApplicationRole { Name = "SuperAdmin" };

                rolemanager.Create(role);
            }


            if (!context.Users.Any(u => u.UserName == "superadmin@gmail.com"))
            {

                var admin1 = new ApplicationUser { UserName = "superadmin@gmail.com", Money = 100000000, EmailConfirmed = true, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "superadmin@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };

                manager.Create(admin1, "Abcdef123!@#");
                manager.AddToRole(admin1.Id, "SuperAdmin");
            }


            if (!context.Users.Any(u => u.UserName == "linhhuynh@gmail.com") && !context.Users.Any(u => u.UserName == "admin@gmail.com"))
            {

                var admin1 = new ApplicationUser
                {
                    UserName = "admin@gmail.com",
                    Money = 100000000,
                    EmailConfirmed = true,
                    DateRegister = DateTime.Now,
                    FullName = "Linh Huỳnh",
                    Email = "admin@gmail.com",
                    Gender = 0,
                    Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"),
                    Address = "Quận 8",
                    PhoneNumber = "0123456789"
                };
                var admin2 = new ApplicationUser { UserName = "linhhuynh@gmail.com", Money = 100000000, EmailConfirmed = true, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "linhhuynh@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };
                manager.Create(admin2, "Abcdef123!@#");
                manager.Create(admin1, "Abcdef123!@#");
                manager.AddToRole(admin1.Id, "Admin");
                manager.AddToRole(admin2.Id, "Admin");
            }

            if (!context.Users.Any(u => u.UserName == "user1@gmail.com"))
            {
                var user1 = new ApplicationUser { UserName = "user1@gmail.com", Money = 100000000, EmailConfirmed = true, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "user1@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };
                var user2 = new ApplicationUser { UserName = "user2@gmail.com", EmailConfirmed = true, Money = 100000000, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "user2@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };
                var user3 = new ApplicationUser { UserName = "user3@gmail.com", EmailConfirmed = true, Money = 100000000, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "user3@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };
                var user4 = new ApplicationUser { UserName = "user4@gmail.com", EmailConfirmed = true, Money = 100000000, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "user4@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };
                var user5 = new ApplicationUser { UserName = "user5@gmail.com", EmailConfirmed = true, Money = 100000000, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "user5@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };
                var user6 = new ApplicationUser { UserName = "user6@gmail.com", EmailConfirmed = true, Money = 100000000, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "user6@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };
                var user7 = new ApplicationUser { Id = "cb9d277b-73d3-465f-92db-5c85a86fab0d", UserName = "user7@gmail.com", Money = 100000000, EmailConfirmed = true, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "admin@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };
                var user8 = new ApplicationUser { UserName = "user8@gmail.com", EmailConfirmed = true, Money = 100000000, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "user7@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };
                var user9 = new ApplicationUser { UserName = "user9@gmail.com", EmailConfirmed = true, Money = 100000000, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "user8@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };
                var user10 = new ApplicationUser { UserName = "user10@gmail.com", Money = 100000000, EmailConfirmed = true, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "user9@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };
                var user11 = new ApplicationUser { UserName = "user11@gmail.com", Money = 100000000, EmailConfirmed = true, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "user10@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };

                manager.Create(user1, "Abcdef123!@#");
                manager.Create(user2, "Abcdef123!@#");
                manager.Create(user3, "Abcdef123!@#");
                manager.Create(user4, "Abcdef123!@#");
                manager.Create(user5, "Abcdef123!@#");
                manager.Create(user6, "Abcdef123!@#");
                manager.Create(user7, "Abcdef123!@#");
                manager.Create(user8, "Abcdef123!@#");
                manager.Create(user9, "Abcdef123!@#");
                manager.Create(user10, "Abcdef123!@#");
                manager.Create(user11, "Abcdef123!@#");


            }
            context.Categories.AddOrUpdate(
                new Category() { Id = 1, Name = "Phụ kiện" },
                new Category() { Id = 2, Name = "Điện thoại" },
                new Category() { Id = 3, Name = "Máy tính, laptop" },
                new Category() { Id = 4, Name = "Khác" }
             );
            var user12 = new ApplicationUser { UserName = "linhhuynh@gmail.com", Money = 100000000, EmailConfirmed = true, DateRegister = DateTime.Now, FullName = "Linh Huỳnh", Email = "linhhuynh@gmail.com", Gender = 0, Birthday = DateTime.Parse("1/1/1993 12:00:00 AM"), Address = "Quận 8", PhoneNumber = "0123456789" };
            manager.Create(user12, "Abcdef123!@#");
            context.Products.AddOrUpdate(
                new Product() { Id = 1, Name = "Điện thoại 1", CategoryId = 1, Price = 1231231231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Checking, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123", },
                new Product() { Id = 2, Name = "Điện thoại 2", CategoryId = 1, Price = 231231231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Selling, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123" },
                new Product() { Id = 3, Name = "Điện thoại 3", CategoryId = 1, Price = 31231231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Selling, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123" },
                new Product() { Id = 4, Name = "Điện thoại 4", CategoryId = 1, Price = 1231231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Selling, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123" },
                new Product() { Id = 5, Name = "Điện thoại 5", CategoryId = 1, Price = 231231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Selling, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123" },
                new Product() { Id = 6, Name = "Điện thoại 6", CategoryId = 1, Price = 31231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Selling, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123" },
                new Product() { Id = 7, Name = "Điện thoại 7", CategoryId = 1, Price = 1231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Denied, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123" },
                new Product() { Id = 8, Name = "Điện thoại 8", CategoryId = 1, Price = 12123231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Denied, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123"},
                new Product() { Id = 9, Name = "Điện thoại 9", CategoryId = 1, Price = 122131, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Sold, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123" },
                new Product() { Id = 10, Name = "Điện thoại 10", CategoryId = 1, Price = 13123231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Sold, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123" }
                );

            context.AuctionProducts.AddOrUpdate(
                new AuctionProduct() { Id = 11, Name = "Điện thoại 1", CategoryId = 1, Price = 1231231231, CurrentPrice = 1231231231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Checking, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123",StartTime = DateTime.Now , EndTime = DateTime.Now.AddDays(2) },
                new AuctionProduct() { Id = 13, Name = "Điện thoại 2", CategoryId = 1, Price = 231231231, CurrentPrice = 231231231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Selling, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123", StartTime = DateTime.Now, EndTime = DateTime.Now.AddDays(2) },
                new AuctionProduct() { Id = 14, Name = "Điện thoại 3", CategoryId = 1, Price = 31231231, CurrentPrice = 31231231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Selling, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123", StartTime = DateTime.Now, EndTime = DateTime.Now.AddDays(2) },
                new AuctionProduct() { Id = 15, Name = "Điện thoại 4", CategoryId = 1, Price = 1231231, CurrentPrice = 1231231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Selling, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123", StartTime = DateTime.Now, EndTime = DateTime.Now.AddDays(2) },
                new AuctionProduct() { Id = 16, Name = "Điện thoại 5", CategoryId = 1, Price = 231231, CurrentPrice = 231231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Selling, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123", StartTime = DateTime.Now, EndTime = DateTime.Now.AddDays(2) },
                new AuctionProduct() { Id = 12, Name = "Điện thoại 6", CategoryId = 1, Price = 31231, CurrentPrice = 31231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Selling, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123", StartTime = DateTime.Now, EndTime = DateTime.Now.AddMinutes(111) },
                new AuctionProduct() { Id = 17, Name = "Điện thoại 7", CategoryId = 1, Price = 1231, CurrentPrice = 1231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Denied, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123", StartTime = DateTime.Now, EndTime = DateTime.Now.AddMinutes(2) },
                new AuctionProduct() { Id = 18, Name = "Điện thoại 8", CategoryId = 1, Price = 12123231, CurrentPrice = 12123231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Denied, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123", StartTime = DateTime.Now, EndTime = DateTime.Now.AddMinutes(2) },
                new AuctionProduct() { Id = 19, Name = "Điện thoại 9", CategoryId = 1, Price = 122131, CurrentPrice = 122131, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Sold, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123", StartTime = DateTime.Now, EndTime = DateTime.Now.AddMinutes(31) },
                new AuctionProduct() { Id = 20, Name = "Điện thoại 10", CategoryId = 1, Price = 13123231, CurrentPrice = 13123231, DateCreate = DateTime.Now, ProductStatus = ProductStatus.Sold, UserId = "cb9d277b-73d3-465f-92db-5c85a86fab0d", Description = "123123123", StartTime = DateTime.Now, EndTime = DateTime.Now.AddDays(2) }
             );
            context.Photos.AddOrUpdate(
                new Photo() { Id = 1, ProductId = 1, URL = "0.jpg" },
                new Photo() { Id = 2, ProductId = 2, URL = "0.jpg" },
                new Photo() { Id = 3, ProductId = 3, URL = "0.jpg" },
                new Photo() { Id = 4, ProductId = 4, URL = "0.jpg" },
                new Photo() { Id = 5, ProductId = 5, URL = "0.jpg" },
                new Photo() { Id = 6, ProductId = 6, URL = "0.jpg" },
                new Photo() { Id = 7, ProductId = 7, URL = "0.jpg" },
                new Photo() { Id = 8, ProductId = 8, URL = "0.jpg" },
                new Photo() { Id = 9, ProductId = 9, URL = "0.jpg" },
                new Photo() { Id = 10, ProductId = 10, URL = "0.jpg" },
                new Photo() { Id = 11, ProductId = 11, URL = "0.jpg" },
                new Photo() { Id = 12, ProductId = 12, URL = "0.jpg" },
                new Photo() { Id = 13, ProductId = 13, URL = "0.jpg" },
                new Photo() { Id = 14, ProductId = 14, URL = "0.jpg" },
                new Photo() { Id = 15, ProductId = 15, URL = "0.jpg" },
                new Photo() { Id = 16, ProductId = 16, URL = "0.jpg" },
                new Photo() { Id = 17, ProductId = 17, URL = "0.jpg" },
                new Photo() { Id = 18, ProductId = 18, URL = "0.jpg" },
                new Photo() { Id = 19, ProductId = 19, URL = "0.jpg" },
                new Photo() { Id = 20, ProductId = 20, URL = "0.jpg" }
                );

            //context.Products.AddOrUpdate(
            //new Product() { Id = 1,Name="Điện thoại Samsung",CategoryId=2,UserId= }
            //        );
        }
    }
}